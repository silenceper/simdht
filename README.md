##simDHT:
A DHT crawler, is not quite based on Kademlia, written by Python, the code is very very simple.

##dependencies:
1. [bencode](https://pypi.python.org/pypi/bencode/1.0)

##start simDHT
```bash
python simDHT.py
```

##notice:
1. when max_node_qsize=10000, out bandwidth=1.5M/s

##live demo
1. [MagnetMaster](http://cilidashi.com)
